<?php

namespace Tests\Feature;

use App\Models\User;
use App\Models\UserGroup;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class UserGroupTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Assert that the groups the user has access to can be fetched
     */
    public function testFetch()
    {
        $firstUser  = factory(User::class)->create();
        $secondUser = factory(User::class)->create();
        $thirdUser  = factory(User::class)->create();

        Auth::login($firstUser);

        /** @var UserGroup $firstGroup */
        $firstGroup  = factory(UserGroup::class)->create();
        /** @var UserGroup $secondGroup */
        $secondGroup = factory(UserGroup::class)->create();

        $firstGroup->users()->sync([$firstUser->id, $secondUser->id]);
        $secondGroup->users()->sync([$thirdUser->id, $secondUser->id]);

        $this->get('/api/user-groups')
            ->assertStatus(200)
            ->assertJsonCount(2);
    }
}
