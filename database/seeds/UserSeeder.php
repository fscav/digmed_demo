<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
                'id'         => 1,
                'name'       => 'John Doe',
                'email'      => 'john.doe@digtest.de',
                'password'   => Hash::make(Str::random()),
                'permission_read'   => true,
                'permission_create' => true,
                'permission_update' => true,
                'permission_delete' => true,
                'export_type'       => 1
            ],
            [
                'id'         => 2,
                'name'       => 'Jane Doe',
                'email'      => 'jane.doet@digtest.de',
                'password'   => Hash::make(Str::random()),
                'permission_read'   => true,
                'permission_create' => true,
                'permission_update' => true,
                'permission_delete' => false,
                'export_type'       => 1
            ],
            [
                'id'         => 3,
                'name'       => 'Digmed User',
                'email'      => 'digmed.user@digtest.de',
                'password'   => Hash::make('password'),
                'permission_read'   => true,
                'permission_create' => true,
                'permission_update' => true,
                'permission_delete' => true,
                'export_type'       => 1
            ],
            [
                'id'         => 4,
                'name'       => 'Digmed Readonly',
                'email'      => 'digmed.readonly@digtest.de',
                'password'   => Hash::make('password'),
                'permission_read'   => true,
                'permission_create' => false,
                'permission_update' => false,
                'permission_delete' => false,
                'export_type'       => 2
            ],
            [
                'id'         => 5,
                'name'       => 'Digmed Norights',
                'email'      => 'digmed.norights@digtest.de',
                'password'   => Hash::make('password'),
                'permission_read'   => false,
                'permission_create' => false,
                'permission_update' => false,
                'permission_delete' => false,
                'export_type'       => 2
            ]
        ]);
    }
}
