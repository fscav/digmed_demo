<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->insert([
            [
                'id'       => 1,
                'title'     => 'Hello World!',
                'content'    => 'Hello World! My name is John Doe.',
                'user_id'    => 1,
                'created_at' => Carbon::yesterday()->toDateTimeString()
            ],
            [
                'id'       => 2,
                'title'     => 'Hello World!',
                'content'    => 'Hello World! My name is Jane Doe.',
                'user_id'    => 2,
                'created_at' => Carbon::yesterday()->toDateTimeString()
            ],
            [
                'id'       => 3,
                'title'     => 'My car broke down',
                'content'    => 'Hello, I hit a boar with my car and now it does not start anymore. Please help!',
                'user_id'    => 1,
                'created_at' => Carbon::now()->toDateTimeString()
            ]
        ]);
    }
}
