<?php


namespace App\Http\Controllers;


use App\Models\Post;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class PostController extends Controller
{
    /**
     * Get all posts
     * @return JsonResponse
     */
    public function fetch(): JsonResponse
    {
        return response()->json(
            Post::with('user')->latest('created_at')->get(),
            200
        );
    }

    /**
     * @todo Update this method to return a file in the format defined in the user's settings (export_format)
     * @return Response
     */
    public function export(): Response
    {
        //TODO: Generate content and file name some other way
        $fileName = "stub.txt";
        $content = "stub";

        return response($content)
            ->header('Content-Type', 'text/plain')
            ->header('Content-Disposition', "attachment; filename=\"$fileName\"")
            ->header('Content-Length', strlen($content));
    }
}
