<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Post;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Post::class, function (Faker $faker, array $params) {
    return [
        'title' => $params['title'] ?? $faker->title,
        'content' => $params['content'] ?? $faker->text,
        'user_id' => $params['user_id'] ?? $faker->numberBetween([1, 10000])
    ];
});
