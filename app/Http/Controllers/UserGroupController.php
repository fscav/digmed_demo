<?php


namespace App\Http\Controllers;


use App\Models\UserGroup;
use Illuminate\Http\JsonResponse;

class UserGroupController extends Controller
{
    /**
     * Get all groups
     * @return JsonResponse
     */
    public function fetch(): JsonResponse
    {
        return response()->json(UserGroup::all(), 200);
    }
}
