<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class UserGroupSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('user_groups')->insert([
            [
                'id'         => 1,
                'name'       => 'Digmed and Friends',
                'created_at' => Carbon::now()->toDateTimeString()
            ],
            [
                'id'         => 2,
                'name'       => 'Doe Family',
                'created_at' => Carbon::now()->toDateTimeString()
            ]
        ]);

        DB::table('user_user_group')->insert([
            [
                'user_id'       => 1,
                'user_group_id' => 1
            ],
            [
                'user_id'       => 3,
                'user_group_id' => 1
            ],
            [
                'user_id'       => 4,
                'user_group_id' => 1
            ],
            [
                'user_id'       => 1,
                'user_group_id' => 2
            ],
            [
                'user_id'       => 2,
                'user_group_id' => 2
            ]
        ]);
    }
}
