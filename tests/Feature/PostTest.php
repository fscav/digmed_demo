<?php

namespace Tests\Feature;

use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Auth;
use Tests\TestCase;

class PostTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Assert that the posts the user has access to can be fetched
     */
    public function testFetch()
    {
        $firstUser  = factory(User::class)->create();
        $secondUser = factory(User::class)->create();
        $thirdUser  = factory(User::class)->create();

        Auth::login($firstUser);

        factory(Post::class, 5)->create(['user_id' => $firstUser->id]);
        factory(Post::class, 3)->create(['user_id' => $secondUser->id]);
        factory(Post::class, 2)->create(['user_id' => $thirdUser->id]);

        $this->get('/api/posts')
            ->assertStatus(200)
            ->assertJsonCount(10);
    }

    /**
     * Assert that the posts the user has access to can be exported
     */
    public function testExport()
    {
        $firstUser  = factory(User::class)->create();
        $secondUser = factory(User::class)->create();
        $thirdUser  = factory(User::class)->create();

        Auth::login($firstUser);

        factory(Post::class, 5)->create(['user_id' => $firstUser->id]);
        factory(Post::class, 3)->create(['user_id' => $secondUser->id]);
        factory(Post::class, 2)->create(['user_id' => $thirdUser->id]);

        $response = $this->get('/api/posts/export');

        $response->assertStatus(200);
        $this->assertEquals("stub", $response->getContent());
    }
}
