# Demo application

This (incomplete) application allows user to share posts which then appear on a board for all related users to see.

Some features are still missing and must be added.

The application is available in a containerized development environment (docker) and does not require a local PHP
installation. To use it, make sure docker and [docker-compose](https://docs.docker.com/compose/) are installed, then run
the ```init``` script.

The development environment ist provided by vessel, an external dependency. You can use the ```vessel``` script to
perform some actions in the containers. These actions are documented [here](https://vessel.shippingdocker.com/docs/everyday-usage/).

The user interface should then be available at http://localhost and any change in the code should immediately impact the
running application. The database will be filled with some useful data.

To log in, use the following credentials:

- Normal user: digmed.user@digtest.de / password
- "Readonly" user: digmed.readonly@digtest.de / password
- User without rights: digmed.norights@digtest.de / password
