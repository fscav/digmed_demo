<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\UserGroup;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(UserGroup::class, function (Faker $faker, array $params) {
    return [
        'name' => $params['name'] ?? $faker->name
    ];
});
